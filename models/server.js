require('dotenv').config()
const cors = require('cors')

const express = require('express')


class Server {

    constructor(){
        //Configutación 
        this.app = express();
        this.port = process.env.PORT;
        this.usuariosPath = '/api/usuarios';

        //Middelwares
        this.middelwares();

        //Rutas
        this.routes();
    }

    middelwares(){

        //Configuración de CORS
        this.app.use(cors());
        //Lectura y parceo del body 
        this.app.use(express.json());
        //Directorio publico
        this.app.use(express.static('public'));
    }

    routes(){
        this.app.use( this.usuariosPath, require('../routes/user.router') )
    }

    listen(){
        this.app.listen( this.port, () => {
            console.log(`Example app listening at http://localhost:${this.port}`)
        })
    }

}

module.exports = Server;