

const usuariosGet = (req, res) => {

    const {nombre, apellido, edad} =  req.query
    res.status(403  ).json({
        message: 'Hola mundo desde peticion get - controller',
        nombre,
        apellido,
        edad
    });
}

const usuariosPost = (req, res) => {

    const id = req.params.id; 
    res.status(403  ).json({
        message: 'Hola mundo desde peticion post - controller con id ' + id
    });
}


const usuariosPut = (req, res) => {

    const { nombre, apellido } = req.body; 
    res.status(200).json({
        message: 'Hola mundo desde peticion put - controller',
        nombre,
        apellido
    });
}

const usuariosDelete = (req, res) => {
    res.status(403  ).json({
        message: 'Hola mundo desde peticion delete - controller'
    })
}


module.exports = {
    usuariosGet,
    usuariosPost,
    usuariosPut,
    usuariosDelete
}