const { Router } = require('express');
const { usuariosGet, usuariosPost, usuariosPut, usuariosDelete} = require('../controllers/user.controller')
const router = Router();

router.get('/', usuariosGet)

router.post('/:id', usuariosPost)

router.put('/', usuariosPut)

router.delete('/', usuariosDelete)


module.exports = router;